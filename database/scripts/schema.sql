CREATE TABLE user_account (
    uin serial PRIMARY KEY,
    nickname varchar(255) UNIQUE NOT NULL,
    password varchar(255) NOT NULL,
    email varchar(255) UNIQUE NOT NULL,
    avatar bytea
);

CREATE TABLE user_relationship (
    relating_user_id integer,
    related_user_id integer,
    relationship varchar(255) NOT NULL,
    PRIMARY KEY (relating_user_id, related_user_id),
    FOREIGN KEY (relating_user_id) REFERENCES user_account(uin),
    FOREIGN KEY (related_user_id) REFERENCES user_account(uin)
);

SELECT setval('user_account_uin_seq', 100000000, true);
