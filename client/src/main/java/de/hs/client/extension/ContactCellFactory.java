package de.hs.client.extension;

import de.hs.client.model.Contact;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class ContactCellFactory implements Callback<ListView<Contact>, ListCell<Contact>> {
    @Override
    public ListCell<Contact> call(ListView<Contact> contactListView) {
        return new ContactCell();
    }
}
