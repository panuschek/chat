package de.hs.client.extension;

import de.hs.chatlib.packet.ChatMessagePacket;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListCell;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

import java.io.IOException;

public class MessageCell extends ListCell<ChatMessagePacket> {

    @FXML
    private Text header;
    @FXML
    private Text content;
    @FXML
    private AnchorPane anchorPane;

    FXMLLoader fxmlLoader;

    @Override
    public void updateItem(ChatMessagePacket packet, boolean empty) {
        super.updateItem(packet, empty);

            if(empty || packet == null) {

                setText(null);
                setGraphic(null);

            } else {
                if (fxmlLoader == null) {
                    fxmlLoader = new FXMLLoader(getClass().getResource("/view/messageList.fxml"));
                    fxmlLoader.setController(this);

                    try {
                        fxmlLoader.load();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                header.setText(packet.getFrom());
                content.setText(packet.getBody());

                header.setWrappingWidth(anchorPane.getPrefWidth());
                header.setUnderline(true);
                content.setWrappingWidth(anchorPane.getPrefWidth());

                anchorPane.setPrefHeight(content.getBoundsInParent().getHeight() + header.getBoundsInParent().getHeight());
                anchorPane.setPrefWidth(content.getBoundsInLocal().getWidth());

                AnchorPane.setTopAnchor(content, header.getBoundsInParent().getHeight());

                setText(null);
                setGraphic(anchorPane);
            }
        }
    }
