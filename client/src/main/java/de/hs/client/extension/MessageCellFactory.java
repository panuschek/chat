package de.hs.client.extension;

import de.hs.chatlib.packet.ChatMessagePacket;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class MessageCellFactory implements Callback<ListView<ChatMessagePacket>, ListCell<ChatMessagePacket>> {

    @Override
    public ListCell<ChatMessagePacket> call(ListView<ChatMessagePacket> chatMessagePacketListView) {
        return new MessageCell();
    }
}
