package de.hs.client.extension;

import de.hs.client.controller.ControllerBase;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SceneManager {
  private final Stage rootStage;
  private final Stage notificationStage;

  public SceneManager(Stage rootStage, Stage notificationStage) {
    if (rootStage == null) {
      throw new IllegalArgumentException();
    }
    this.rootStage = rootStage;
    this.notificationStage = notificationStage;
  }

  private final Map<String, Scene> scenes = new HashMap<>();

  public void switchScene(String url) {
    switchScene(url, false);
  }

  public void switchScene(String url, boolean newInstance) {
    if(newInstance) {
      scenes.remove(url);
    }

    Scene scene = scenes.computeIfAbsent(url, u -> {
      FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/" + u + ".fxml"));
      try {
        Parent p = loader.load();
        ControllerBase controller = loader.getController();
        controller.setSceneManager(this);
        controller.setNotificationStage(notificationStage);
        return new Scene(p);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
    rootStage.setScene(scene);
  }
}
