package de.hs.client.extension;

import de.hs.chatlib.packet.OnlineStatus;
import de.hs.client.model.Contact;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ContactCell extends ListCell<Contact> {
  @FXML
  private Text txtNickname;

  @FXML
  private HBox container;

  @FXML
  private ImageView statusImageView;

  @FXML
  private ImageView notificationImageView;

  @SneakyThrows
  @Override
  public void updateItem(Contact contact, boolean empty) {
    super.updateItem(contact, empty);

    if (empty || contact == null) {
      this.setText(null);
      this.setGraphic(null);
      return;
    }
    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/friendListCell.fxml"));
    fxmlLoader.setController(this);
    fxmlLoader.load();

    txtNickname.setText(contact.getNickname());
    notificationImageView.managedProperty().bind(notificationImageView.visibleProperty());
    notificationImageView.setVisible(contact.isHasPendingMessages());

    if (contact.getOnlineStatus() == OnlineStatus.ONLINE) {
      statusImageView.setImage(new Image("/assets/images/icq_online.png"));
    } else {
      statusImageView.setImage(new Image("/assets/images/icq_offline.png"));
    }

    this.setText(null);
    this.setGraphic(container);
  }
}
