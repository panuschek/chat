package de.hs.client.service;

import de.hs.chatlib.packet.ChatMessagePacket;

import java.util.List;

public interface MessageService {
    /**
     * Used to store messages with a specific user.
     * @param uin The id of the user to whom the chat protocol belongs to
     * @param messages The message packets.
     */
    void saveMessages(Long uin, List<ChatMessagePacket> messages);
    List<ChatMessagePacket> loadMessages(Long uin);
}
