package de.hs.client.controller;

import com.esotericsoftware.kryonet.Connection;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.hs.chatlib.packet.ChatMessagePacket;
import de.hs.chatlib.packet.Packet;
import de.hs.client.extension.MessageCellFactory;
import de.hs.client.model.Contact;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import lombok.SneakyThrows;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ChatController extends ControllerBase {
    @FXML
    public ImageView imgOtherAvatar;

    @FXML
    public ImageView imgMyAvatar;

    private Contact chatPartner;
    private Contact me;
    private ObservableList<ChatMessagePacket> observableChatMessagePackets;
    private Gson gson = new GsonBuilder().create();
    private File messagesFile;

    @FXML
    private TextArea messageTextArea;
    @FXML
    private ListView messageListView;

    @SneakyThrows
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        observableChatMessagePackets = FXCollections.observableList(new ArrayList());
        messageListView.setCellFactory(new MessageCellFactory());
        messageListView.setItems(observableChatMessagePackets);
    }

    @Override
    public void packetReceived(Connection connection, Packet packet) {
        if (packet instanceof ChatMessagePacket) {
            if (((ChatMessagePacket) packet).getFrom().equals(chatPartner.getNickname())) {
                Platform.runLater(() -> {
                    observableChatMessagePackets.add((ChatMessagePacket) packet);
                    try {
                        insertMessagesIntoFile((ChatMessagePacket) packet);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    messageListView.scrollTo(observableChatMessagePackets.size() - 1);
                });
            }
        }
    }

    public void readMesssagesFromFile(File file) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String messageString;
        while ((messageString = bufferedReader.readLine()) != null) {
            ChatMessagePacket message = gson.fromJson(messageString, ChatMessagePacket.class);
            observableChatMessagePackets.add(message);
            messageListView.scrollTo(observableChatMessagePackets.size() - 1);
        }
        bufferedReader.close();
    }

    public void insertMessagesIntoFile(ChatMessagePacket packet) throws IOException {
        String path = chatPartner.getNickname() + ".json";
        File file = new File(path);
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String gsonPacket = gson.toJson(packet) + "\n";
        FileWriter fileWriter = new FileWriter(file, true);
        fileWriter.write(gsonPacket);
        fileWriter.close();
    }

    public void sendMessage(KeyEvent event) throws IOException {
        if (event.getCode() == KeyCode.ENTER) {
            if(messageTextArea.getText().length() > 2000) {
                Alert alert = new Alert(Alert.AlertType.WARNING, "Exceeded maximum length of 2000 characters. Chill!");
                alert.showAndWait();
                messageTextArea.setText("");
                return;
            }
            ChatMessagePacket message = new ChatMessagePacket(me.getNickname(), chatPartner.getNickname(), messageTextArea.getText().substring(0, messageTextArea.getText().length() - 1));
            if (message.getBody().length() > 0) {
                observableChatMessagePackets.add(message);
                serverConnection.sendPacket(message);
                insertMessagesIntoFile(message);
                messageTextArea.setText("");
                messageListView.scrollTo(observableChatMessagePackets.size() - 1);
            } else {
                messageTextArea.setText("");
            }
        }
    }

    public void setChatPartner(Contact chatPartner) {
        this.chatPartner = chatPartner;
        File otherAvatarFile = new File(chatPartner.getUin() + ".png");

        if (otherAvatarFile.exists()) {
            imgOtherAvatar.setImage(new Image(otherAvatarFile.toURI().toString()));
        }
    }

    public void setMe(Contact me) {
        this.me = me;
        File myAvatarFile = new File(me.getUin() + ".png");

        if (myAvatarFile.exists()) {
            imgMyAvatar.setImage(new Image(myAvatarFile.toURI().toString()));
        }
    }

    public void setMessagesFile(File file) {
        messagesFile = file;
    }

    public void close() {
        serverConnection.removeListener(this);
    }

    public void onReport() {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("JSON files (*.json)", "*.json");
        fileChooser.getExtensionFilters().add(extFilter);

        File selectedFile = fileChooser.showSaveDialog(messageListView.getScene().getWindow());
        if (selectedFile == null)
            return;

        try {
            selectedFile.createNewFile();
            BufferedWriter writer = new BufferedWriter(new FileWriter(selectedFile));
            for (ChatMessagePacket message: observableChatMessagePackets) {
                writer.write(gson.toJson(message) + "\n");
            }
            writer.close();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Couldn't create report.", ButtonType.OK);
            alert.showAndWait();
        }
    }
}
