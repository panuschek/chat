package de.hs.client.controller;

import com.esotericsoftware.kryonet.Connection;
import de.hs.chatlib.packet.LoginRequestPacket;
import de.hs.chatlib.packet.LoginResponsePacket;
import de.hs.chatlib.packet.Packet;
import de.hs.client.ApplicationProperties;
import de.hs.client.model.Contact;
import de.hs.client.service.UserService;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

public class LoginController extends ControllerBase {

    private final String PROP_KEY_HOSTNAME = "server.hostname";
    private final String PROP_KEY_PORT = "server.port";
    @FXML
    public TextField txtNickname;
    @FXML
    public PasswordField txtPassword;
    @FXML
    public Label lblErrorMessage;

    private UserService userService;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        userService = UserService.getInstance();
        Properties applicationProperties = ApplicationProperties.get();
        String hostname = applicationProperties.getProperty(PROP_KEY_HOSTNAME);
        int port = Integer.parseInt(applicationProperties.getProperty(PROP_KEY_PORT));

        try {
            if(!serverConnection.isConnected()) {
                serverConnection.connect(hostname, port);
            }

            if(userService.getMe() != null && userService.getMe().getUin() != null) {
                txtNickname.setText(userService.getMe().getUin().toString());
            }
        } catch (IOException e) {
            lblErrorMessage.setText("Connection failed.\nPlease try again later.");
        }
    }

    @Override
    public void packetReceived(Connection connection, Packet packet) {
        if(packet instanceof LoginResponsePacket) {
            var loginReponse = (LoginResponsePacket)packet;
            if(!loginReponse.isSuccess()) {
                Platform.runLater(() -> lblErrorMessage.setText("Nickname/Password incorrect."));
                return;
            }

            userService.setMe(new Contact(loginReponse.getUin(), loginReponse.getNickname()));
            Platform.runLater(() -> sceneManager.switchScene("home"));
        }
    }

    public void onLoginButtonClicked() {
        doLogin();
    }

    public void onRegisterLinkClick() {
        sceneManager.switchScene("register");
    }

    public void onEnterKey(ActionEvent actionEvent) {
        doLogin();
    }

    private void doLogin() {
        try {
            Long uin = Long.parseLong(txtNickname.getText());
            serverConnection.sendPacket(new LoginRequestPacket(uin, txtPassword.getText()));
        } catch (Exception e) {
            Platform.runLater(() -> lblErrorMessage.setText("Please enter a valid UIN."));
        }
    }
}
