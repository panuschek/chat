package de.hs.chatlib.packet;

public enum OnlineStatus {
  ONLINE,
  OFFLINE,
  UNKNOWN
}
