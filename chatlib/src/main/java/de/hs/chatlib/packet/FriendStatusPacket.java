package de.hs.chatlib.packet;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FriendStatusPacket extends Packet {
  private Long uin;
  private String nickname;
  private OnlineStatus status;
}
