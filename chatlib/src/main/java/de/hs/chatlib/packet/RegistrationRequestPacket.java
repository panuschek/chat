package de.hs.chatlib.packet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegistrationRequestPacket extends Packet {
  private String nickname;
  private String password;
  private String email;
  private byte[] avatar;
}
