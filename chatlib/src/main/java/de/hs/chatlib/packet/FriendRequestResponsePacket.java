package de.hs.chatlib.packet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FriendRequestResponsePacket extends Packet {
  private Long from;
  private Long to;
  private boolean accepted;
}
