package de.hs.chatlib.packet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginResponsePacket extends Packet {
  private Long uin;
  private String nickname;
  private boolean success;
}
