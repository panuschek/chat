package de.hs.chatlib;

import com.esotericsoftware.kryo.Kryo;
import de.hs.chatlib.packet.*;

public class ChatLib {
  public static void init(Kryo kryo) {
    kryo.register(RegistrationRequestPacket.class);
    kryo.register(RegistrationResponsePacket.class);
    kryo.register(LoginRequestPacket.class);
    kryo.register(LoginResponsePacket.class);
    kryo.register(FriendStatusPacket.class);
    kryo.register(FriendRequestPacket.class);
    // kryo.register(FriendListEntryPacket.class);
    kryo.register(FriendListRequestPacket.class);
    kryo.register(ChatMessagePacket.class);
    kryo.register(OnlineStatus.class);
    kryo.register(AvatarRequest.class);
    kryo.register(AvatarResponse.class);
    kryo.register(byte[].class);
  }
}
