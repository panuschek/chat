package de.hs.testclient;

import javafx.fxml.Initializable;
import javafx.stage.Stage;

public interface PrimaryStageAware extends Initializable {
  void setPrimaryStage(Stage primaryStage);
}
