package de.hs.testclient;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class TestClientApplication extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader loader = new FXMLLoader();

        loader.setControllerFactory((Class<?> type) -> {
            try {
                Object controller = type.newInstance();
                if (controller instanceof PrimaryStageAware) {
                    ((PrimaryStageAware) controller).setPrimaryStage(stage);
                }
                return controller ;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });

        var temp = getClass().getResourceAsStream("test.fxml");
        Parent home = loader.load(temp);
        stage.setTitle("Test Client");
        stage.setScene(new Scene(home, 600, 400));
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}