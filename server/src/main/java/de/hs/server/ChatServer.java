package de.hs.server;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import de.hs.chatlib.ChatLib;
import de.hs.chatlib.packet.*;
import de.hs.server.db.DatabaseException;
import de.hs.server.db.UserRepository;
import de.hs.chatlib.packet.OnlineStatus;
import de.hs.server.model.Contact;
import de.hs.server.model.Relationship;
import de.hs.server.model.User;
import de.hs.server.model.UserRelationship;
import de.hs.server.security.AuthenticationProvider;
import lombok.extern.slf4j.Slf4j;

import javax.mail.MessagingException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
public class ChatServer extends Listener {
  private final int port;
  private final AuthenticationProvider authenticationProvider;
  private final UserRepository userRepository;
  private final MailService mailService;
  private final Map<Long, Connection> users;

  private Server server;

  public ChatServer(int port, AuthenticationProvider authenticationProvider, UserRepository userRepository) {
    this.port = port;
    this.authenticationProvider = authenticationProvider;
    this.userRepository = userRepository;
    this.users = new HashMap<>();
    this.mailService = MailService.getInstance();
  }

  @Override
  public void received(Connection connection, Object packet) {
    if(packet instanceof RegistrationRequestPacket) {
      handleRegistrationPacket(connection, (RegistrationRequestPacket)packet);
    }

    if(packet instanceof LoginRequestPacket) {
      handleLoginPacket(connection, (LoginRequestPacket)packet);
    }

    if(packet instanceof AvatarRequest) {
      handleAvatarRequestPacket(connection, (AvatarRequest)packet);
    }

    if(packet instanceof ChatMessagePacket) {
      handleChatMessagePacket((ChatMessagePacket)packet);
    }

    if(packet instanceof FriendRequestPacket) {
      handleFriendRequestPacket((FriendRequestPacket)packet);
    }

    if(packet instanceof FriendRequestResponsePacket) {
      handleFriendRequestResponsePacket((FriendRequestResponsePacket)packet);
    }

    if(packet instanceof FriendListRequestPacket) {
      handleFriendListRequestPacket(connection);
    }

    if(packet instanceof FriendStatusPacket){
      handleFriendStatusPacket();
    }
  }

  private void handleAvatarRequestPacket(Connection connection, AvatarRequest avatarRequest) {
    byte[] avatar = userRepository.findAvatarByUin(avatarRequest.getUin());
    connection.sendTCP(new AvatarResponse(avatarRequest.getUin(), avatar));
  }

  private void handleFriendStatusPacket(){

  }

  private void handleFriendListRequestPacket(Connection connection) {
    var myUin = getUinByConnectionId(connection.getID());
    for(Contact contact : userRepository.findAllContacts()) {
      if(contact.getUin() == myUin)
        continue;

      connection.sendTCP(new FriendStatusPacket(contact.getUin(), contact.getNickname(), users.containsKey(contact.getUin()) ? OnlineStatus.ONLINE : OnlineStatus.OFFLINE));
    }
  }

  private void handleFriendRequestResponsePacket(FriendRequestResponsePacket packet) {
    if(packet.isAccepted()) {
      UserRelationship userRelationship1 = new UserRelationship(packet.getFrom(), packet.getTo(), Relationship.FRIENDS);
      UserRelationship userRelationship2 = new UserRelationship(packet.getTo(), packet.getFrom(), Relationship.FRIENDS);

      userRepository.saveRelationship(userRelationship1);
      userRepository.saveRelationship(userRelationship2);

      var contact1 = userRepository.findContactByUin(userRelationship1.getRelatingUserId());
      var contact2 = userRepository.findContactByUin(userRelationship2.getRelatingUserId());

      var user1 = users.get(contact1.getUin());
      var user2 = users.get(contact2.getUin());

      user1.sendTCP(new FriendStatusPacket(contact2.getUin(), contact2.getNickname(), OnlineStatus.ONLINE));
      user2.sendTCP(new FriendStatusPacket(contact1.getUin(), contact1.getNickname(), OnlineStatus.ONLINE));
    } else {
      userRepository.deleteRelationship(packet.getTo(), packet.getFrom());
    }
  }

  private void handleFriendRequestPacket(FriendRequestPacket friendRequestPacket) {
    // TODO: Create a packet buffer when user is offline
    var recipient = users.get(friendRequestPacket.getTo());
    recipient.sendTCP(friendRequestPacket);
    UserRelationship userRelationship = new UserRelationship(
            friendRequestPacket.getFrom(),
            friendRequestPacket.getTo(),
            Relationship.PENDING);

    userRepository.saveRelationship(userRelationship);
  }

  private void handleRegistrationPacket(Connection connection, RegistrationRequestPacket registrationRequestPacket) {
    User user = User.builder()
            .nickname(registrationRequestPacket.getNickname())
            .password(registrationRequestPacket.getPassword())
            .email(registrationRequestPacket.getEmail())
            .build();

    try {
      user = userRepository.saveUserWithAvatar(user, registrationRequestPacket.getAvatar());
      connection.sendTCP(new RegistrationResponsePacket(true, null, user.getUIN()));
      mailService.sendConfirmationMail(user);

      for (Map.Entry<Long, Connection> entry : users.entrySet()) {
        if (entry.getKey().equals(user.getUIN()))
          continue;

        entry.getValue().sendTCP(new FriendStatusPacket(user.getUIN(), user.getNickname(), users.containsKey(user.getUIN()) ? OnlineStatus.ONLINE : OnlineStatus.OFFLINE));
      }

    } catch (DatabaseException e) {
      connection.sendTCP(new RegistrationResponsePacket(false, e.getMessage(), null));
    } catch (MessagingException e) {
      connection.sendTCP(new RegistrationResponsePacket(false, "Couldn't send activation mail. Good luck!", null));
    }
  }

  private void handleChatMessagePacket(ChatMessagePacket chatMessagePacket) {
    log.debug("Received ChatMessagePacket from {} to {}.", chatMessagePacket.getFrom(), chatMessagePacket.getTo());

    var receiver = userRepository.findByNickname(chatMessagePacket.getTo());
    if(users.containsKey(receiver.getUIN())) {
      users.get(receiver.getUIN()).sendTCP(chatMessagePacket);
    }
  }

  private void handleLoginPacket(Connection connection, LoginRequestPacket loginRequestPacket) {
    log.debug("Received LoginRequestPacket from {}.", loginRequestPacket.getUin());

    boolean authSuccess = authenticationProvider
            .authenticate(loginRequestPacket.getUin(), loginRequestPacket.getPassword());

    log.debug("AuthSuccess={} for {}.", authSuccess, loginRequestPacket.getUin());

    if(users.containsKey(loginRequestPacket.getUin())) authSuccess = false;

    var user = userRepository.findByUin(loginRequestPacket.getUin());

    if(authSuccess) {
      users.forEach((key, value) -> {
        if (!key.equals(user.getUIN())) {
          value.sendTCP(new FriendStatusPacket(user.getUIN(), user.getNickname(), OnlineStatus.ONLINE));
        }
      });
      users.put(user.getUIN(), connection);
    }

    Long uin = user == null ? null : user.getUIN();
    String nickname = user == null ? null : user.getNickname();

    connection.sendTCP(new LoginResponsePacket(uin, nickname, authSuccess));
  }

  @Override
  public void disconnected(Connection connection) {
    log.info("{} disconnected.", connection.getID());
    var userId = getUinByConnectionId(connection.getID());
    var user = userRepository.findContactByUin(userId);
    users.remove(userId);

    try {
      broadcast(new FriendStatusPacket(userId, user.getNickname(), OnlineStatus.OFFLINE));
    } catch(Exception e) {
      log.info("Exception on disconnect {}", e.getMessage());
    }
  }

  /**
   * Starts the server in a new thread, undefined behaviour if thread is already running.
   * @throws IOException Thrown when something goes wrong with Kryo
   */
  public void start() throws IOException {
    // TODO: What do we do if the thread is still running?
    server = new Server(65000, 65000);
    ChatLib.init(server.getKryo());
    server.start();
    server.bind(port);
    server.addListener(this);
  }

  public void stop() {
    log.info("Stopping ChatServer");
    server.stop();
  }

  /**
   * Broadcasts the given packet to all the users in the user list.
   * @param packet The packet to be sent.
   */
  private void broadcast(Packet packet) {
    users.values().forEach(con -> con.sendTCP(packet));
  }

  private Long getUinByConnectionId(int connectionId) {
    AtomicReference<Long> user = new AtomicReference<>();
    users.forEach((key, value) -> {
      if (value.getID() == connectionId)
        user.set(key);
    });

    return user.get();
  }
}
