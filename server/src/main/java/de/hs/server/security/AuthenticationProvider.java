package de.hs.server.security;

import de.hs.server.db.UserRepository;

public class AuthenticationProvider {
  protected UserRepository userRepository;

  public AuthenticationProvider(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  public boolean authenticate(Long uin, String password) {
    var user = userRepository.findByUin(uin);
    return user != null && user.getPassword().equals(password);
  }
}
