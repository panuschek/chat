package de.hs.server;

import de.hs.server.db.PostgresUserRepository;
import de.hs.server.model.User;
import de.hs.server.security.AuthenticationProvider;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class ServerApplication  {
    public static void main(String[] args) throws IOException {
        final int port = 1337;

        var userRepository = new PostgresUserRepository();
        ChatServer chatServer = new ChatServer(port, new AuthenticationProvider(userRepository), userRepository);
        chatServer.start();
    }
}
