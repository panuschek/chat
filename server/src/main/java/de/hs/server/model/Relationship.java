package de.hs.server.model;

public enum Relationship {
  FRIENDS,
  PENDING,
  BLOCKED
}
