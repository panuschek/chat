package de.hs.server;

import lombok.SneakyThrows;

import java.util.Properties;

public class ApplicationProperties {
  private static Properties properties;

  @SneakyThrows
  public static Properties get() {
    if(properties == null) {
      properties = new Properties();
      var inputStream = ApplicationProperties.class.getClassLoader().getResourceAsStream("config.properties");
      properties.load(inputStream);
    }

    return properties;
  }
}
