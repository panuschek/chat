package de.hs.server.db;

public class DatabaseException extends Exception {
  public DatabaseException(String message) {
    super(message);
  }
}
