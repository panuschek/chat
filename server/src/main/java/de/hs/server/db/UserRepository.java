package de.hs.server.db;

import de.hs.server.model.Contact;
import de.hs.server.model.User;
import de.hs.server.model.UserRelationship;

import java.util.List;

public interface UserRepository {
  User findByUin(Long uin);
  User findByNickname(String nickname);
  Contact findContactByUin(Long uin);
  Contact findContactByNickname(String nickname);
  List<Contact> findAllContacts();
  User save(User user) throws DatabaseException;
  List<Contact> findFriendsByNickname(String nickname) throws DatabaseException;
  List<Contact> findFriendsByUin(Long uin) throws DatabaseException;
  UserRelationship saveRelationship(UserRelationship userRelationship);
  void deleteRelationship(Long relatingUserId, Long relatedUserId);
  void saveAvatar(Long uin, byte[] avatar);
  User saveUserWithAvatar(User user, byte[] avatar) throws DatabaseException;
  byte[] findAvatarByUin(Long uin);
}
