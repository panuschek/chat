package de.hs.server.db;

import de.hs.server.ApplicationProperties;
import de.hs.server.model.Contact;
import de.hs.server.model.User;
import de.hs.server.model.UserRelationship;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Slf4j
public class PostgresUserRepository implements UserRepository {
  private final String PROPERTY_KEY_DB_URL = "database.url";
  private final String PROPERTY_KEY_DB_USER = "database.user";
  private final String PROPERTY_KEY_DB_PASSWORD = "database.password";

  private final Properties properties;

  public PostgresUserRepository() {
    this.properties = ApplicationProperties.get();
  }

  @Override
  public User findByUin(Long uin) {
    try(Connection connection = openConnection()) {
      var statement = connection.createStatement();
      var query = String.format("SELECT * FROM user_account WHERE uin='%s'", uin);
      var resultSet = statement.executeQuery(query);

      if(resultSet.next()) {
        return new User(
                resultSet.getLong("uin"),
                resultSet.getString("nickname"),
                resultSet.getString("password"),
                resultSet.getString("email"),
                new ArrayList<>());
      }

    } catch (SQLException ex) {
      ex.printStackTrace();
    }
    return null;
  }

  @Override
  public User findByNickname(String nickname) {
    try(Connection connection = openConnection()) {
      var statement = connection.createStatement();
      var query = String.format("SELECT * FROM user_account WHERE nickname='%s'", nickname);
      var resultSet = statement.executeQuery(query);

      if(resultSet.next()) {
        return new User(
                resultSet.getLong("uin"),
                resultSet.getString("nickname"),
                resultSet.getString("password"),
                resultSet.getString("email"),
                new ArrayList<>());
      }

    } catch (SQLException ex) {
      ex.printStackTrace();
    }
    return null;
  }

  @Override
  public Contact findContactByUin(Long uin) {
    try(Connection connection = openConnection()) {
      var statement = connection.createStatement();
      var resultSet = statement.executeQuery("SELECT * FROM user_account WHERE uin=" + uin);

      if(resultSet.next()) {
        return new Contact(
                resultSet.getLong("uin"),
                resultSet.getString("nickname")
        );
      }

    } catch (SQLException ex) {
      ex.printStackTrace();
    }
    return null;
  }

  @Override
  public Contact findContactByNickname(String nickname) {
    try(Connection connection = openConnection()) {
      var statement = connection.createStatement();
      var resultSet = statement.executeQuery("SELECT * FROM user_account WHERE nickname=" + nickname);

      if(resultSet.next()) {
        return new Contact(
                resultSet.getLong("uin"),
                resultSet.getString("nickname")
        );
      }

    } catch (SQLException ex) {
      ex.printStackTrace();
    }
    return null;
  }

  @Override
  public List<Contact> findAllContacts() {
    List<Contact> contacts = new ArrayList<>();
    try(Connection connection = openConnection()) {
      var statement = connection.createStatement();
      var resultSet = statement.executeQuery("SELECT uin, nickname FROM user_account");

      while(resultSet.next()) {
        contacts.add(new Contact(
                resultSet.getLong("uin"),
                resultSet.getString("nickname")
        ));
      }

    } catch (SQLException ex) {
      ex.printStackTrace();
    }
    return contacts;
  }

  @Override
  public User save(User user) throws DatabaseException {
    try(Connection connection = openConnection()) {
      var statement = connection.createStatement();
      var query = String.format("INSERT INTO user_account (nickname, password, email) VALUES ('%s', '%s', '%s')",
              user.getNickname(),
              user.getPassword(),
              user.getEmail());

      statement.executeUpdate(query);
      return findByNickname(user.getNickname());
    } catch (SQLException ex) {
      String exceptionMessage = "An unknown error occured.";
      if(ex.getMessage().contains("user_account_nickname_key")) {
        exceptionMessage = "Nickname already taken.";
      } else if(ex.getMessage().contains("user_account_email_key")) {
        exceptionMessage = "Email is already linked to an account.";
      }
      log.warn("SQL ERROR {}", ex.getMessage());
      throw new DatabaseException(exceptionMessage);
    }
  }

  @Override
  public List<Contact> findFriendsByNickname(String nickname) throws DatabaseException {
    var user = findByNickname(nickname);
    return findFriendsByUin(user.getUIN());
  }

  @Override
  public List<Contact> findFriendsByUin(Long uin) throws DatabaseException {
    List<Contact> contacts = new ArrayList<>();
    try(Connection connection = openConnection()) {
      var statement = connection.createStatement();
      var query = String.format("SELECT related_user_id FROM user_relationship WHERE relating_user_id='%s' AND WHERE relationship=friends");
      var resultSet = statement.executeQuery(query);

      while(resultSet.next()) {
        var user = findByUin(resultSet.getLong("related_user_id"));
        contacts.add(new Contact(user.getUIN(), user.getNickname()));
      }

    } catch (SQLException ex) {
      ex.printStackTrace();
      throw new DatabaseException("Error while querrying user.");
    }

    return contacts;
  }

  @Override
  public UserRelationship saveRelationship(UserRelationship userRelationship) {
    try(Connection connection = openConnection()) {
      var statement = connection.createStatement();
      var query = String.format("INSERT INTO user_relationship (relating_user_id, related_user_id, relationship) VALUES ('%s', '%s', '%s')",
              userRelationship.getRelatingUserId(),
              userRelationship.getRelatedUserId(),
              userRelationship.getRelationship());

      statement.executeUpdate(query);
      return userRelationship;
    } catch (SQLException ex) {
      ex.printStackTrace();
    }

    return null;
  }

  @Override
  public void deleteRelationship(Long relatingUserId, Long relatedUserId) {
    try(Connection connection = openConnection()) {
      var statement = connection.createStatement();
      var query = String.format("DELETE ROM user_relationship WHERE relating_user_id='%s' AND WHERE related_user_id='%s'",
              relatingUserId,
              relatedUserId);

      statement.executeUpdate(query);
    } catch (SQLException ex) {
      ex.printStackTrace();
    }
  }

  @Override
  public void saveAvatar(Long uin, byte[] avatar) {
    try(Connection connection = openConnection()) {
      var query = String.format("UPDATE user_account SET avatar=? WHERE uin='%s'",
              uin);

      var statement = connection.prepareStatement(query);
      statement.setBytes(1, avatar);
      statement.executeUpdate(query);
    } catch (SQLException ex) {
      ex.printStackTrace();
    }
  }

  @Override
  public User saveUserWithAvatar(User user, byte[] avatar) throws DatabaseException {
    try(Connection connection = openConnection()) {
      var query = String.format("INSERT INTO user_account (nickname, password, email, avatar) VALUES ('%s', '%s', '%s', ?)",
              user.getNickname(),
              user.getPassword(),
              user.getEmail());

      PreparedStatement statement = connection.prepareStatement(query);
      statement.setBytes(1, avatar);
      statement.executeUpdate();
      return findByNickname(user.getNickname());
    } catch (SQLException ex) {
      String exceptionMessage = "An unknown error occured.";
      if(ex.getMessage().contains("user_account_nickname_key")) {
        exceptionMessage = "Nickname already taken.";
      } else if(ex.getMessage().contains("user_account_email_key")) {
        exceptionMessage = "Email is already linked to an account.";
      }
      log.warn("SQL ERROR {}", ex.getMessage());
      throw new DatabaseException(exceptionMessage);
    }
  }

  @Override
  public byte[] findAvatarByUin(Long uin) {
    try(Connection connection = openConnection()) {
      var statement = connection.createStatement();
      var resultSet = statement.executeQuery(String.format("SELECT avatar FROM user_account WHERE uin='%s'", uin));

      if(resultSet.next()) {
        return resultSet.getBytes("avatar");
      }

    } catch (SQLException ex) {
      ex.printStackTrace();
    }
    return null;
  }

  private Connection openConnection() throws SQLException {
    return DriverManager.getConnection(
            properties.getProperty(PROPERTY_KEY_DB_URL),
            properties.getProperty(PROPERTY_KEY_DB_USER),
            properties.getProperty(PROPERTY_KEY_DB_PASSWORD));
  }
}
